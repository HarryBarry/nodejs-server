import express from 'express';
const router = express.Router();

import * as MainController from '../controllers/main';

router.get('/', MainController.getHomePage);

router.get('/category', MainController.getCatPage);

router.get('/signup', MainController.getSignupPage);

export default router;
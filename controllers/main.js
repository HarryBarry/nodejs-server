export function getHomePage(req, res) {
    res.json('Home page');
}

export function getCatPage(req, res) {
    res.json('Category page');
}

export function getSignupPage(req, res) {
    res.json('Sign up page!');
}
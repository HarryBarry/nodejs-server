const config = {
    database: {host:'mongodb://localhost', port: ':27017', name:'/ecommerce'},
    session : {secret: '{secret}', name: 'session_id', saveUninitialized: true, resave: true},
    serverPort: 3000
};

export default config;
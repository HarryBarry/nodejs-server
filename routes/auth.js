import express from 'express';
const router = express.Router();

import * as AuthController from '../controllers/auth';

router.post('/signup', AuthController.signup);


export default router;
import express from 'express';
import morgan from 'morgan';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import session from 'express-session';
import cookieParser from 'cookie-parser';
import flash from 'express-flash';
import bluebird from 'bluebird';
import helmet from 'helmet';

// Configs
import config from './config';

// Middlewares include
import errorHandler from './middlewares/errorHandler';

// Routes
import mainRoutes from './routes/main';
import authRoutes from './routes/auth';

const app = express();

// Promise add && mongoDB connect
mongoose.Promise = bluebird;
mongoose.connect(config.database.host+config.database.port+config.database.name, function(err) {
    if (err) throw err;
    console.log(`---->>> DB: Connected to the database on port ${config.database.port}`);
});

// Middleware
app.use(express.static(__dirname+'public'));
app.use(helmet());
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(session(config.session));
app.use(flash());

// Routes include
app.use(mainRoutes);
app.use(authRoutes);

// Errors handle
app.use(function(req, res) {
    res.status(404);
    res.json({title:'Internal Server Error', status: 404});
});

// Server start
app.listen(config.serverPort, function (err) {
   if (err) throw err;
    console.log(`---->>> SERVER: Server is running on port ${config.serverPort}`);
});
